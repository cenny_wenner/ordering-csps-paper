%\CCTODO {Ensure that predicates and other mathematical objects are in
%  italic if they are elsewhere}


% NOTE BTW is a Garey and Johnson problems - BTW=MS1. Also appears in
% Kann's compendium. MAS is not?  Should we explained how MAS is
% reduced to an OCSP?  Do we get any improvement for the complementary
% problem Minimum Feedback Arc Set?


\section{Introduction}

% Outline of the structure in comments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Introducing the project
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% - we study NP hardness of approximating OCSPs
% - towards:
%   + understanding and characterizing approximation resistance
% - such a nice characterization is known assuming the UGC

We study the $\NP$-hardness of approximating a rich class of
optimization problems known as the \emph{Ordering Constraint
  Satisfaction Problems}~(OCSPs).  An instance of an OCSP is described
by a set of variables $\outputV$ and a set of \emph{local ordering
  constraints} $\outputE$ where each constraint is specified by a set of
variables and a set of permitted permutations on these variables.  The
objective is to find a permutation of the variables $\outputV$ maximizing the
fraction of constraints satisfied by the induced local permutations.
%and the optimal fraction is called the \emph{value} of the instance.

\begin{wrapfigure}{r}{3.4cm}
%\lncs{
%}
%\fbox{
%\begin{minipage}{3.2cm}
\begin{center}
\begin{tikzpicture}[->,node distance=1.05cm]
\tikzset{EdgeStyle/.style={->,}}
\Vertex{x}
\EA(x){z}
\EA(z){y}
\NO(z){a}
\SO(z){b}
\Edges(b,x,a,z,b,y,a)
\end{tikzpicture}
\end{center}
\vspace{-0.5ex}
%\end{minipage}
%}
\caption[]{An \MAS instance with value $5/6$.}
\label{fig:masgadget}
\label{fig:intro_mas_gadget}
\end{wrapfigure}

A simple example of an OCSP is the problem
\textproblem{Maximum Acyclic Subgraph}~(\MAS) where one is given a directed graph $G =
(V, A)$ with the task of finding an acyclic subgraph of $G$ containing
as many the arcs as possible.  Phrased as an OCSP, $V$ is the set of
variables and each arc $u \to v$ is a constraint ``$u \prec v$''
dictating that $u$ should precede $v$. The maximum fraction of
constraints simultaneously satisfiable by an ordering of $V$ is then
exactly the fraction of edges in a largest acyclic subgraph.
%normalized by the number of constraints $|A|$.
Since each constraint in an \MAS{} instance involves exactly two
variables, it is an OCSP of \emph{width} two. Another example of an OCSP is
the \textproblem{Maximum Betweenness}~(\MaxBTW) problem ~\cite{GJ79}.
In this width-three OCSP, a constraint on a triplet of variables $(x,y,z)$
is satisfied by the local ordering $x \prec z \prec y$
and its reverse, $y \prec z \prec x$; in other words, $z$ has to be
between $x$ and $y$, giving rise to the name of the problem.

Determining the optimal value of an \MAS instance is $\NP$-hard and one
turns to approximations. An algorithm is called a $c$-approximation
if, when applied to an instance $\instance$, the algorithm is guaranteed to produce
an ordering $\ordr$ satisfying a fraction of constraints within a factor $c$ of
the optimum, \ie, $\val(\ordr; \instance) \ge c \cdot \val(\instance)$.
%at least a fraction $c \cdot \val(\instance)$ of the constraints.
% - we are not using the following in this paper /c
%We also
%allow randomized algorithms in which case the value of the ordering
%found by the algorithm only needs to be $c \cdot \val(\instance)$ in
%expectation over the coin tosses of the algorithm.
As in the case of classical constraint satisfaction, every OCSP
admits a naive approximation algorithm which picks an ordering
of $\outputV$ uniformly at random without even looking at the
constraints.  For \MAS, this algorithm yields a $1/2$-approximation in
expectation as each constraint is satisfied with probability $1/2$
for a random ordering
%and, as with classical CSPs, can be derandomized into a deterministic
%algorithm through conditional expectations.

Surprisingly, there is evidence that this mindless procedure
achieves the best polynomial-time approximation constant:
assuming Khot's Unique Games Conjecture~(UGC)~\cite{Khot02a}, \MAS is
hard to approximate within $1/2+\epsilon$ for every $\epsilon >
0$~\cite{GuruswamiMR08,GuruswamiHMRC11}.  An OCSP is called
\emph{approximation resistant} if it exhibits this behavior, \ie, if
it is \NP-hard to improve upon the guarantee of the random-ordering
algorithm by some absolute constant $c > 0$.  In fact, the
results of \cite{GuruswamiHMRC11} are more general and contrasting
with classical constraint satisfaction: assuming the
UGC, they prove that \emph{every} OCSP of bounded width is
approximation resistant.

In many cases -- such as for \textproblem{Vertex Cover},
\textproblem{Max Cut}, and as we just mentioned,
for all OCSPs -- the UGC implies optimal
$\NP$-hard inapproximability constants
which are not known without the conjecture.  For instance, the
problems \MAS and \MaxBTW were to date only known to be $\NP$-hard to
approximate within $65/66+\epsilon$~\cite{Newman01} and
$47/48+\epsilon$~\cite{ChorS98}, which comes far from matching the
respective random-assignment thresholds of $1/2$ and $1/3$.  In
fact, while the UGC implies that all OCSPs are approximation
resistant, there were no results proving $\NP$-hard approximation
resistance of an OCSP prior to this work.  In contrast, there is a
significant body of work on \NP-hard approximation resistance of
classical Constraint Satisfaction
Problems~(CSPs)~\cite{Hastad01,SamorodnitskyT00,EngebretsenH08,Chan13}.
Furthermore, the UGC is still very much open and recent algorithmic
advances have given rise to subexponential algorithms for Unique
Games~\cite{AroraBS10,BarakBHKSZ12} putting the conjecture in
question.  Several recent works have also been aimed at bypassing the
UGC for natural problems by providing comparable results without
assuming the conjecture~\cite{GuruswamiRSW12,Chan13}.

% Recent work 

%  but there are also
% problems such as \textproblem{Max 3-Lin-2} and \textproblem{3-Not All
%   Equal} (3NAE) permitting non-trivial
% approximations~\cite{GoemansW95, Zwick98}.

\subsection{Results}\label{sec:results}%

\begin{table}
\begin{center}
\newcommand*\supcite[1]{ {$^{\textrm{\cite{#1}} } $} }%
\begin{tabular}{llllll}
Problem &%
%Density &%
Approx. factor &%
\UG-inapprox. &%
\NP-inapprox.&%
This work\\%
\hline%
%
\MAS &%
%$1/2$ &%
$1/2 + \Omega(\nicefrac{1}{\log{n}})$\supcite{CharikarMM07}&%
$1/2 + \epsilon$\supcite{GuruswamiMR08} &%
$65/66 + \epsilon$\supcite{Newman01} &%
$14/15 + \epsilon$%
\\%
%
\MaxBTW &%
%$1/3$ &%
$1/3$ &%
$1/3 + \epsilon$\supcite{CharikarGM09} &%
$47/48 + \epsilon$\supcite{ChorS98} &%
$1/2 + \epsilon$%
\\%
%
\MaxNBTW &%
%$2/3$ &%
$2/3$ &%
$2/3 + \epsilon$\supcite{CharikarGM09} &%
- &%
$2/3 + \epsilon$%
\\%
%
$m$-OCSP &%
%$1/m!$ &%
$1/m!$ &%
$1/m! + \epsilon$\supcite{GuruswamiHMRC11}  &%
- &%
$1 / \floor{m / 2}! + \epsilon$%
\end{tabular}%
\end{center}%
\caption{Prior results and improvements.}%
\label{table:ocsp_results}%
\end{table}

% cTODO we have not defined what almost satisfiable etc means.  

% cTODO if MAS was introduced by GJ (not just a natural complement of
% feedback arc set) then we should mention it too 

% cNOTE Hassin and Rubenstein lists a number of MAS appears such as
% being complete for permutation optimization problems Max SNP[pi]
% [PY], some special cases which can bve approximated better, and
% applications.  cTODO where was MAS approximability first considered?
% CNOTE! I don't think the problems were first introduced in GJ, it
% might simply be listed in it.

In this work we obtain improved $\NP$-hardness of approximating
various OCSPs.  While a complete characterization such as in the UG
regime still eludes us, our results improve the knowledge of what we
believe are four important flavors of OCSPs; see
\expref{Table}{table:ocsp_results} for a summary of the present state of
affairs.

We address the two most studied OCSPs: \MAS and \MaxBTW. For \MAS,
we show a factor $(14/15+\epsilon)$-inapproximability improving the factor from
$65/66+\epsilon$~\cite{Newman01}.  For \MaxBTW, we show a factor
$(1/2+\epsilon)$-inapproximability improving from $47/48+\epsilon$~\cite{ChorS98}.

\begin{theorem}\label{thm:mas_result}%
  For every $\epsilon > 0$, it is $\NP$-hard to distinguish between
  \MAS instances with value at least $15/18-\epsilon$ from instances
  with value at most $14/18 + \epsilon$.
\end{theorem}

\begin{theorem}\label{thm:mbtw_result}%
  For every $\epsilon > 0$, it is $\NP$-hard to distinguish between
  \MaxBTW instances with value at least $1-\epsilon$ from instances with
  value at most $1/2 + \epsilon$.
\end{theorem}

The above two results are inferior to what is known assuming the UGC
and in particular do not prove approximation resistance.  We introduce
the \textproblem{Maximum Non-Betweenness}~(\MaxNBTW) problem which
accepts the \emph{complement} of the predicate in \MaxBTW.  This
predicate accepts four of the six permutations on three
elements and thus a random ordering satisfies two thirds of the constraints
in expectation.  We show that this is optimal up to smaller-order
terms.

\begin{theorem}\label{thm:mnbtw_result}%
  For every $\epsilon > 0$, it is $\NP$-hard to distinguish between
  \MaxNBTW instances with value at least $1-\epsilon$ from instances with
  value at most $2/3 + \epsilon$.
\end{theorem}

Finally, we address the approximability of a generic width-$m$ OCSP as
a function of the width $m$.  In the CSP world, the generic
version is called $m$-CSP and we call the ordering version $m$-OCSP.
We devise a simple predicate, ``$2t$-Same Order'' (\SOPred),
on $m=2t$ variables that is satisfied only if the first
$t$ elements are relatively ordered exactly as the last $t$ elements.
A random ordering satisfies only a fraction $1/t!$ of the constraints and we
prove that this is essentially optimal, implying a
$(1/\floor{m/2}!+\epsilon)$-factor inapproximability of $m$-OCSP.

\begin{theorem}\label{thm:mocsp_result}%
  For every $\epsilon > 0$ and integer $m \geq 2$, it is $\NP$-hard to
  distinguish $m$-\textrm{OCSP} instances with value at least $1 - \epsilon$
  from value at most $1 / \floor{m / 2}! + \epsilon$.
\end{theorem}


% At the outset, our inapproximability is via a
% standard long-code reduction from Label Cover~(LC) relying on
% noise and partial independence to decouple queries to different tables
% unless the tables are significantly correlated with, and in consequence suggesting,
% a good labelling.

% However, OCSPs differ from CSPs in two crucial aspects. First, we do
% not permit OCSP instances to be \emph{folded} by which we mean
% shifting values of variables by constraint-specific
% constants. Folding is commonplace and a key component in showing
% inapproximability results for CSPs. In a standard PCP construction
% for a CSP establishing uselessness, one instantiates constraints
% with all possible shifts of arguments, adjusting the queries to
% counteract the shift. In the following analysis, one argues,
% depending on the composition, that the arguments of the constraint
% can be made independent unless the queried tables imply a good
% labelling. As a consequence of queries being independent and each
% argument is independently shifted, the result is a uniform
% distribution over the possible assignments to the variables with an
% expected value equal to that of an algorithm outputting a random
% assignment.

% % ctodo Max Cut claim should reference Håstad and Trevisan et al
% In this light, OCSPs are more similar to problems such as
% \emph{\textproblem{Max Cut}} and \emph{\textproblem{Homogenous
% Linear Equations}} (Hom Lin). In \textproblem{Max Cut}, no folding
% of constraints is available while solutions for \textproblem{Hom
% Lin} must be balanced. The best $\NP$-hard approximation results for
% \textproblem{Max Cut} are to this day via a gadget reduction from
% the approximation resistant CSP \textproblem{Max 3-Lin-2}. However,
% for Hom-Lin over GF(2) with three variables per equation, Holmerin
% and Khot was able to establish approximation resistance unless $\NP
% \subseteq \cap_{\epsilon > 0} \DTIME(2^{n^\epsilon})$, owing to a
% new outer verifier having both so called \emph{mixing} and
% \textit{smoothness} properties which with the global balancing
% constraint essentially implies locally-balanced tables.

% % ctodo we should mention that we use bucketing/coalescing as used
% % previously in eg guruswami and the UG OCSP problems. Note how ours
% % differs.

% [The remainder of this section needs some work] Our analysis carries
% some similarity to these constructions

% Our construction carries some similarities to this approach, we

% An ordering, treated as a mapping onto $\Z$ differs from an
% assignment to a CSP in that the domain of the latter is bounded by a
% constant whereas the domain of an ordering is linear in the number
% of variables. Directly employing conventional analysis machinery

% introduces fundamental hurdles in employing tools developed in
% handling CSPs.  We circumvent this by arguing that one can
% \emph{bucket and partition} the ordering locally into a bounded
% number of pieces if we can .

% Also note on how these large domains may be what makes all OCSPs
% approximation resistant.



% \paragraph{Techniques.}%
% At the outset, the inapproximability results are from a standard
% Long Code reduction from Label Cover
% % The reductions to OCSPs, all, follow the standard dictatorship test
% % paradigm: the reduction is from an instance of Label Cover and
% % replaces each vertex by a long code, and edges by the dictatorship
% % gadget.  However, our analysis deviates from that of CSPs due to
% % additional complications arising from analysing functions that are not
% % on a fixed domain.  In fact, the techniques are different even from
% % the corresponding inapproximabilities obtained assuming the UGC due to
% % added compliations arising from the projection constraints.

% TODO, See result section
% At the outset,
% standard long-code reduction from Label Cover~(LC) relying on
% noise and partial independence to decouple queries to different tables
% unless the tables are significantly correlated with, and in consequence suggesting,
% a good labelling.

% Approximating bounded occurrence ordering CSPs - Yuan Zhoul, Venkatesan Guruswami

% \subsection{Related Work}\label{sec:relatedWork}

% Approximability of Ordering CSPs has been of consider 
% \textproblem{Max Acyclic Subgraph}~ is a classic optimization problem,
% figuring in Karp’s early list of \NP-hard problems~\cite{Karp}; the
% problem is also complete for the class of permutation optimization
% problems, \textproblem{Max SNP$[\pi]$}~(see \cite{maxSNP}), that can
% be approximated within a constant factor. \MAS is \NP-hard even on
% graphs with maximum degree $3$ and was shown to be \NP-hard to
% approximate within a factor better than $66/65$~\cite{Newman01}.
% Turning to algorithmic results, the problem is known to be efficiently
% solvable on planar graphs~\cite{planarMAS} and reducible flow
% graphs~\cite{reducibleFlowMAS}. Berger and Shor~\cite{degreeMAS} gave
% a polynomial time algorithm with approximation ratio $2 -
% \Omega(\nicefrac{1}{\sqrt{d}})$ on $d$-degree graphs (the best factor is
% $9/8$ when $d = 3$~\cite{NewmanDegree}.)  The best approximation
% factor for general graphs is $2 - \Omega({\nicefrac{1}{\log
%     n}})$~\cite{CharikarMM07}.  The complementary objective of
% minimizing the number of ``back arcs'', or equivalently deleting the
% minimum number of edges in order to make the graph a DAG, leads to the
% \textproblem{Min Feedback Arc Set (FAS)} problem~(see
% prob. (GI8)~\cite{GJ79}.) This problem admits a factor $O(\log n \log
% \log n )$ approximation algorithm~\cite{SeymourFAS}.

% The \textproblem{Maximum Betweeeness} problem was also introduced by
% Garey Johnson (MS1)~\cite{GJ79}. Opatrny~\cite{Opatrny} showed that
% the decision version of the betweenness problem is NP-complete. This
% problem arises naturally when analyzing certain mapping problems in
% molecular biology. For example, it arises when trying to order markers
% on a chromosome, given the results of a radiation hybrid
% experiment~\cite{randDNA}. A computational task of practical
% signifiance in this context is to find a total ordering of the markers
% (the x i in our terminology) that maximizes the number of satisfied
% con- straints. Indeed, betweenness is central in the recent software
% package RHMAPPER. $48/47$ but for satisfiable instances.

% On the approximation side, a random assignment achieves an, under UGC
% optimal constant-factor, $1/3$-approximation in expectation. However,
% for instances where all constraints can be satisfied TODO noted that a
% 1/2 approximation can be found in polynomial time. TODO subsequently
% simplified this by TODO (NOTE is this also the best today?).

% Hassin and Rubenstein \cite{HassinR94} approx $2 - \Omega(1 / \sqrt
% {\Delta(G)}$ of \MAS improved by CMM to $2 - \Omega(1 / \log n)$
% \cite{CharikarMM07} Chor and Sudan\cite{ChorS98} have approx 2 and
% inapprox $48/47 - \eps$ for satisfiable \MaxBTW instances.  No known
% better results for almost sat \MaxBTW instances as far as I know.

% Inapprox 65/66 by Newman\cite{Newman01} via a gadget reduction
% from...?

% For k-csp, we may want to list Engebretsen, ST assuming UGC, Chan
% (and Huang? (perf sat instances)).

% Eng + Holmerin 2005: Odd, should resolve: they list $2^{q -
%   \sqrt{2q-2} - 1/2}$ in a corollary while Chan lists them as $q
% O(\sqrt{q})/q^k$.\cite{EngebretsenH05}

% Chan\cite{Chan13} $q(q − 1)k/q^k$ $O(qk/q^k) for k >= q$

% ST $2k/2^k$ assuming UGC.\cite{SamorodnitskyT09}

\subsection{Proof Overviews}
With the exception of \MAS, our results follow a route which is by now
standard in inapproximability: starting from the optimization problem
\labelcover, we give reductions %to the problems at hand
using
\emph{dictatorship-test} gadgets, also known as long-code tests.
We describe these reductions in the context of
\MaxNBTW to highlight the new techniques in this paper.

The reduction produces an instance $\instance$ of \MaxNBTW from an
instance $\lc$ of \labelcover such that $\val(\instance)
> 1 - \epsilon$ if $\val(\lc) = 1$ while $\val(\instance) < 2/3 + \epsilon$ if
$\val(\lc) \le \eta$ for some $\eta = \eta(\epsilon)$.  By the PCP Theorem and the Parallel
Repetition Theorem~\cite{AroraSafra98,AroraLMSS98,Raz98}, it is
$\NP$-hard to distinguish between $\val(\lc) = 1$ and $\val(\lc) \le
\eta$ for every constant $\eta > 0$ and thus we obtain the result
in \expref{Theorem}{thm:mnbtw_result}.
The core component in this paradigm is the design of a
dictatorship test: a \MaxNBTW instance on $[q]^\lcLeftLabelSet \cup [q]^\lcRightLabelSet$,
for integers $q$ and label sets $\lcLeftLabelSet$ and $\lcRightLabelSet$.
%Let $\pi$ be the map $\lcRightLabelSet \to \lcLeftLabelSet$ that sends $r$ to $\lfloor rL/R \rfloor$.
Let $\pi$ be a map $\lcRightLabelSet \to \lcLeftLabelSet$. Each
constraint is a tuple $(\x, \y, \z)$ where $\x \in [q]^L$, while $\y,
\z \in [q]^R$. The distribution of tuples is obtained as follows.
First, pick $\x$, and $\y$ uniformly at random from $[q]^L$, and
$[q]^R$.  Set $z_j = y_j + x_{\pi(j)} \bmod q$.  Finally, add noise by
independently replacing each coordinate $x_i$, $y_j$ and $z_j$ with a
uniformly random element from $[q]$ with probability $\gamma$.

This test instance has canonical assignments that satisfy almost all
the constraints.  These are obtained by picking an arbitrary $j \in
[R]$, and partitioning the variables into $q$ sets $S_0, \ldots
S_{q-1}$ where $S_t = \set{\x \in [q]^\lcRightLabelSet |\ x_{\pi(j)} = t} \cup
\set{\y \in [q]^\lcLeftLabelSet |\ y_j = t}.$ If a constraint $(\x, \y, \z)$ is
so that $\x \in S_t$, $\y \in S_u$ then $\z \notin S_v$ for any $v \in
\set{t+1, \ldots, u-1}$ except with probability $O(\gamma)$.  This is
because $(a+b)$ mod $q$ is never strictly between $a$ and $b$.
Further, the probability that any two of $\x$, $\y$, and $\z$ fall in
the same set $S_i$ is simply the probability that any two of
$x_{\pi(j)}, y_j$, and $z_j$ are assigned the same value,
which is at most $O(1/q)$.  Thus, ordering the variables such
that $S_0 \prec S_1 \prec \ldots \prec S_{q-1}$ with an arbitrary
ordering of the variables within a set satisfies a fraction $1 - O(1/q)
- O(\gamma)$ constraints.

The proof of \expref{Theorem}{thm:mnbtw_result} requires a partial converse of
the above: every ordering that satisfies more than a fraction $2/3 +
\epsilon$ of the constraints is more-or-less an ordering that depends
on a few coordinates $j$ as above.  This proof involves three steps.
First, we show that there is a $\Gamma = \Gamma(q, \gamma, \beta)$ such that
every ordering $\ordr$ of $\irange{q}^\lcLeftLabelSet$ or $\irange{q}^\lcRightLabelSet$
can be broken into $\Gamma$ sets $S_0, \ldots, S_{\Gamma-1}$
such that one achieves expected value
at least $\val(\ordr) - \beta$ for arbitrarily small $\beta$
by ordering the sets $S_0 \prec \ldots \prec S_{\Gamma-1}$
and within each set ordering elements randomly.
The proof of this ``bucketing''
uses hypercontractivity of noised functions from a finite
domain.  We note that a related bucketing argument is
used in proving inapproximability of OCSPs assuming the
UGC~\cite{GuruswamiMR08,GuruswamiHMRC11}.  Their bucketing argument
is tied to the use of the UGC, where $\card{L} = \card{R}$ for the corresponding
dictatorship test, and does not extend to our setting.  In particular,
our approach yields $\Gamma \gg q$ while they crucially require $\Gamma \ll
q$ in their work.  We believe that our bucketing argument is more general
and a useful primitive.

Then, similarly to \cite{GuruswamiMR08,GuruswamiHMRC11}, the bucketing
argument allows an OCSP to be analyzed as if it were a CSP on a finite
domain, enabling us to use powerful techniques developed in this
setting.  In particular, we show that unless $\val(\lc) > \eta$, the
distribution of constraints $(\x, \y, \z)$ can be regarded as obtained
by sampling $\x$ independently up to an error $\epsilon$ in the payoff; in
other words, $\x$ is ``decoupled'' from $(\y, \z)$.  We note that the
marginal distribution of the tuple $(\y, \z)$ is already symmetric
with respect to swaps: $\Prob{\y = y, \z = z} = \Prob{\y = z, \z =
  y}$.  In order to prove approximation resistance, we combine three
of these dictatorship tests: the \ordinal{$j$} variant has $\x$ as the
\ordinal{$j$}
component of the $3$-tuple.  We show that the combined instance is
symmetric with respect to every swap up to an error $O(\epsilon)$ unless
$\val(\lc) > \eta$.  This implies that the instance has value at
most $2/3 + O(\epsilon)$ hence proving approximation resistance of
\MaxNBTW.

For \MaxBTW and \MaxSO, we do not require the final symmetrization and
instead use a dictatorship test based on a different distribution.
Finally, the reduction to \MAS is a simple gadget reduction from
\MaxNBTW.  For hardness results of width-two predicates, such gadget
reductions presently dominate the scene of classical CSPs and also
define the state of affairs for \MAS.  As an example, the best-known
$\NP$-hard approximation hardness of $16 / 17 + \epsilon$ for
\textproblem{Max Cut}\ is via a gadget reduction from \textproblem{Max
  3-Lin-2}~\cite{Hastad01,TrevisanSSW00}.  The previously best
approximation hardness of \MAS was also via a gadget reduction from
\textproblem{Max 3-Lin-2}~\cite{Newman01}, although with the
significantly smaller gap $65/66 + \epsilon$. By reducing from a
problem more similar to \MAS{}, namely \MaxNBTW{}, we improve to the
approximation hardness to $14/15 + \epsilon$.  The gadget in question
is quite simple and we have in fact already seen it in
\expref{Section}{fig:intro_mas_gadget}.

\paragraph{Organization.} \expref{Section}{sec:preliminaries} sets up the
notation used in the rest of the article. \expref{Section}{sec:dictatorshipTest}
gives a general hardness result based on a test distribution which is
subsequently used in Section\nobreakspace \ref {sec:inapproxRedn} to derive our main results.  The
proof of the soundness of the general hardness reduction is largely given
in Section\nobreakspace \ref {sec:proof of soundness}.

{
\small
\paragraph{Acknowledgment.} We would like to thank \JHastad for
suggesting the topic and for numerous helpful discussions regarding
the same, as well as the anonymous referees who assisted in improving
the legibility of the work.
%We acknowledge ERC Advanced Grant $226203$ and Swedish
%Research Council Grant $621$-$2012$-$4546$ for supporting this project.
}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "orderingNPHardness"
%%% End: 
