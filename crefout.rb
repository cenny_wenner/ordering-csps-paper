for x in Dir["*.tex"]
	next if x[/crefout/]
	newname = x[0...-4] + ".crefout.tex"
	puts "sed twoWiseOrdering.sed #{x} > #{newname}"
	`sed -f twoWiseOrdering.sed #{x} > #{newname}`
	s = File.read(newname)
	s.gsub!(/\\input\s*(?:\{[^\}]*\}|\s\S+)/) do |x|
		if x[/\S[\\\/]/]
			x #subdir
		else
			x.sub('.tex', '.crefout.tex')
		end
	end
	File.open(newname, "w"){|f|f.print s}
end
