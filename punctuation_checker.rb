punct_exempt_envs = ['align', 'gather', 'equation', 'equations', 'enumerate', 'itemize']

for fname in Dir['*.tex']
	s = File.read(fname)
	prevl = ''
	linecount = -1
	for l in s.lines
		linecount += 1
		if m1 = prevl.match(/^(?:(?!\n)\s)*([\w\$][^\n]*\w+)$/)
			if m2 = l.match(/^\\(?:begin|end)\{\s*([^}]*?)\*?\s*\}|^\s*$/i)
				next if punct_exempt_envs.include? m2[1]
				puts "Possible missing punctuation in file '#{fname}' on line #{linecount}:"
				puts "\t" + prevl
				puts "\t" + l
			end
		end
		prevl = l
	end
end
