\section{Analysis of the Reduction}
\label{sec:proof of soundness}

In this section we prove Theorem\nobreakspace \ref {theorem:reduction soundness} which
bounds the value of the instance generated by the reduction in terms of
the decoupled distribution.  Throughout, we fix an LC instance $\lc$,
a predicate $\payoff$, an OCSP instance $\instance$ obtained by
the procedure $\redgeneral$ for a distribution $\baseDist$ and
noise-parameter $\gamma$, and finally an assignment
$A = \set{f_u}_{u \in  \lcLeft} \cup \set{g_v}_{v \in \lcRight}$.

The proof involves three major steps.
First, we show that the assignment functions, which are $\Z$-valued, can be
approximated by functions on finite domains via \emph{bucketing}~(see
Section\nobreakspace \ref {sec:bucketing}).  This approximation makes the analyzed
instance value susceptible to tools
developed in the context of finite-domain
CSPs~\cite{Wenner12,Chan13} which are used in
Section\nobreakspace \ref {sec:test_soundness} to prove the decoupling property of the
dictatorship test.  Finally, this decoupling is extended to the
reduction hence bounding the value of 
\ifdefined\fullVersion
$\instance$ in Section\nobreakspace \ref {sec:reduction soundness}.
\else
$\instance$.
\fi

\def\buckF{B^{(f)}}
\def\buckG{B^{(g)}}
\def\buck{\bucketPred^{(f,g)}}
\def\extF{R^{(f)}}
\def\extG{R^{(g)}}
\def\buckParameter{\Gamma}

\subsection{Bucketing}\label{sec:bucketing}

\def\buckF{B^{(f_u)}} \def\buckG{B^{(g_v)}}%
For an integer $\buckParameter$, we approximate the (possibly non-injective) function $f_u:
Q_1^L \to \Z$ by partitioning the domain into $\buckParameter$ multisets.
Set $q_1 = \card{Q_1}$ and partition the set $Q_1^L$ into sets
$\buckF_1,\ldots,\buckF_{\buckParameter}$ of size
$q_1^L/{\buckParameter}$ such that if $\vx \in \buckF_i$ and $\vy \in
\buckF_j$ for some $i < j$ then $f(\vx) < f(\vy)$.  Note that this is
possible as long as the parameter $\buckParameter$ divides $q_1^L$
which is the case in the applications of this treatise.
Let $\bucketInd[]_u: Q_1^L \to
[{\buckParameter}]$ specify the mapping of points to the bucket
containing it, and $\bucketInd[a]_u: Q_1^L \rightarrow \{0,1\}$ the
indicator of points assigned to $\buckF_a$.  Partition $g_v: Q_2^R \to
\Z$ similarly into buckets $\{\buckG_{a}\}$ obtaining
$\bucketIndR[]_v: Q_2^R \to [{\buckParameter}]$ and $\bucketIndR[a]_v:
Q_2^R \to \bits.$ \def\buckF{B^{(f)}} \def\buckG{B^{(g)}}%

We show that a bucketed version approximates well
the acceptance probability of the dictatorship test
for any edge $e = (u,v)$ from Section\nobreakspace \ref {sec:dictatorshipTest},
\begin{equation*}
  \accP(\dicD) \defeq \E_{(\X, \Y) \from \dicD}[\payoff((f,g) \circ (\X,\Y))].% f(\x[1]), \ldots, f(\x[t]),%
    % g(\y[t+1]), \ldots, g(\y[m-t]))],
\end{equation*}%

%applied to
%an edge $e = (u, v)$ of the LC instance $\lc$
Fix an edge $e = (u, v)$ and put $f = f_u$, $g =
g_v$.  As before, we concisely denote by $(\X, \Y)$ and $(f,g) \circ (\X,\Y)$
the query tuple and assignment tuple,
\[
(\x[1],\ldots,\x[t],\y[t+1],\ldots,\y[m]) \qquad \text{ resp. } \qquad (f(\x[1]), \ldots,f(\x[t]), g(\y[t+1]),\ldots,g(\y[m])).
\]
Define the \emph{bucketed payoff function} $\buck: [{\buckParameter}]^m \to \bndI$
with respect to $f$ and $g$ as
\begin{align}
\buck(a_1,\ldots,a_m) &=
  \mathop{\E_{\x[i] \from \buckF_{a_i};~i\le t}}_{\y[j] \from \buckG_{a_j};~j>t}
  \left[\payoff(\x[1], \dotsc, \x[t], \y[t+1], \dotsc, \y[m])
  \right]\label{eq:bucketPayoff}\\
  \intertext{and the \emph{bucketed acceptance probability}, }
  \bVal(\dicD) &= \Ex[(\X,\Y) \from \dicD]{ \buck( (F,G) \circ (\X, \Y))}.
\label{eq:bucketVal}
\end{align}
In other words, bucketing corresponds to generating a tuple $\vec{a} = (F, G) \circ (\X, \Y)$
and replacing each coordinate $a_i$ with a random value from the bucket which $a_i$ fell in. 
We show that above is close to the true acceptance
probability $\accP(\dicD)$.

\begin{theorem}[Bucketing Preserves Value]\label{prop:bucketing_loss_bound}
  For every predicate $\payoff$, noise parameter $\gamma > 0$,
  projection $\pi: R \to L$,
  distribution $\baseDist$ with uniform marginals, pair of functions $f:
  Q_1^L \to \Z$ and $g: Q_2^R \to \Z$, and bucketing parameter ${\buckParameter}$,
  \[ \left|\accP(\dicD) -%
    \bVal(\dicD) \right| \le m^2 {\buckParameter}^{-\delta}, \]
    for some $\delta = \delta(\gamma, Q) > 0$ with $Q \ge \max \{\card{Q_1}, \card{Q_2}\}.$
\end{theorem}

To prove this, we show that for every choice of $f$ and $g$, including $f=g$,
there is only a few overlapping pairs of buckets, and the probability of hitting
any particular pair is small. For each $a \in \irange{\Gamma}$, let $\extF_a$ be the smallest interval
in $\Z$ containing $\buckF_a$; and similarly $\extG_a$ for $g$.

\begin{lemma}[Few Buckets Overlap]\label{prop:bucket_overlap_bound}% 
  For every integer ${\buckParameter}$ there are at most
  $2{\buckParameter}$ choices of pairs $(a, b) \in [{\buckParameter}]
  \times [{\buckParameter}]$ such that $\extF_{a} \cap \extG_{b} \ne
  \emptyset.$
\end{lemma}
\begin{proof}
  Construct the bipartite intersection graph $G_I = (U_I \cup V_I, E_I)$
  where the vertex sets are disjoint copies of $[{\buckParameter}]$,
  and there is an edge $(a, b) \in U_I \times V_I$ iff $\extF_a \cap \extG_{b} \ne \emptyset$. By
  construction, the intervals $\{ \extF_a \}_a$ are
  disjoint and similarly $\{ \extG_b \}_b$. Consequently, $G_I$
  must be a forest as it does not contain any pair of
  distinct edges $(u,v), (u',v')$ such that $u < u'$ and $v >
  v'$. We conclude that that the number of
  edges and hence intersections is at most $\card{U_I \cup V_I} = 2\Gamma$.
   
  %~ Consequently, a vertex can have at most two neighbors with
  %~ degree greater than one. Let $A$ be the set of degree-one
  %~ vertices. It follows that the maximum degree of the subgraph
  %~ induced by $\comp{A}$ is at most two and
  %~ contains at most $\card{(U_I \cup V_I) \setminus A}$ edges. 
  %~ On the other hand, the number of edges incident to $A$
  %~ is at most $\card{A}$ implying a total of at most
  %~ $\card{U_I \cup V_I} = 2\Gamma$ intersections.
  %~ and $\card{E[
    %~ G_I[\comp{A}]] } \leq \card{U_I+V_I-A}$ while $\card{E(A,
    %~ U_I+V_I)} \leq \card{A}$. Since $\card{E_I} = \ca	rd{ E[
    %~ G_I[\comp{A}]] } + \card{E(A, U_I+V_I)} \leq \card{U_I + V_I - A}
  %~ + \card{A} \leq 2{\buckParameter}.$
\qedendproof

Next, we prove a bound on the probability that a fixed pair of the $m$
queries fall in a specific pair of buckets.  For a distribution $\cD$ over
$Q_1^{\lcLeftLabelSet} \times Q_2^{\lcRightLabelSet}$, define
$\cD^{(\gamma)}$ as the distribution that samples from $\cD$ and for each
of the $\card{\lcLeftLabelSet} + \card{\lcRightLabelSet}$ coordinates
independently with probability $\gamma$ replaces it with a new sample
from $\cD$. The distribution $\cD^{(\gamma)}$ is representative of the projection of
$\dicD$ to two specific coordinates and we show that noise prevents
the buckets from intersecting with good probability.

% \Ctodo{note that this lemma also works with min q but then we should rephrase the proof somewhat.... isn't this what we use at some point however where $q_1$ is 2?}
\begin{lemma}[Bucket Collisions are Unlikely]\label{prop:hyperContractiveBound}%
  Let $\cD$ be a distribution over $Q_1^{\lcLeftLabelSet} \times Q_2^{\lcRightLabelSet}$
  whose marginals are uniform in $Q_1^{\lcLeftLabelSet}$ and $Q_2^{\lcRightLabelSet}$ and
  $\cD^{(\gamma)}$ be as defined above.  For every integer ${\buckParameter}$ and every
  pair of functions $F: Q_1^{\lcLeftLabelSet} \to \bits$ and $G: Q_2^{\lcRightLabelSet} \to
  \bits$ such that $\Ex{F(\x)} = \Ex{G(\y)} = 1/{\buckParameter}$,
  \[ \Ex[(\x,\y) \in \cD^{(\gamma)}]{ F(\x) G(\y) } \le {\buckParameter}^{-(1 + \delta)}\]
  for some $\delta = \delta(\gamma, Q) > 0$ where $Q \ge \min\{\card{Q_1}, \card{Q_2}\}$.
%O(\nicefrac{\gamma q}{(q - 2)\log(q - 1)})$
\end{lemma}
\def\nOp{T_{1-\gamma}}
\begin{proof} Without loss of generality, let $\card{Q_1} = \max\{\card{Q_1}, \card{Q_2}\}$.
Set $q = 2 + \delta' > 2$ as in Lemma\nobreakspace \ref {prop:bestHCBound}, $1/q' = 1 - 1/q$,
  and define $H(\x ) \defeq \Ex[\y | \x ]{\nOp G(\y )}$. Then,
  \begin{align*}
    \Ex[(\x,\y) \in \cD^{(\gamma)}]{ F(\x) G(\y) }
    %= \Ex[(\x,\y) \in D]{ \nOp F(\x) \nOp G(\y) }
    &= \Ex[\x]{ \nOp F(\x) H(\x) }
    \le \norm{ \nOp F }_q \norm { H }_{q'}  %&&(\text{H\"{o}lder's inequality})\\
    \\&\le \norm{F}_2 \norm{H}_{q'}
    \le \norm{F}_2 \norm{\nOp G}_{q'} 
    \le \norm{F}_2 \norm{G}_{q'} 
    %\\&\le \norm{ \nOp F }_q \norm { G }_{q'}% &&(\text{contractivity})\\
    %\\ &&(\text{hypercontractivity})\\
    \\&= {\buckParameter}^{-(1/2 + 1/q')}
    = {\buckParameter}^{-(1 + \nicefrac{\delta'}{2(2+\delta')})},
  \end{align*}
  using the hypercontractivity from Lemma\nobreakspace \ref {prop:bestHCBound} and Jensen's inequality.
  %, convexity of norms, and the contractivity of $\nOp$.
\qedendproof

Note that the above lemma applies to queries to the same function as
well, setting $F = G$, etc.  To complete the proof of
Theorem\nobreakspace \ref {prop:bucketing_loss_bound}, we apply the above lemma to every
distinct pair of the $m$ queries made in $\dicD$ and each of the at
most $2 \buckParameter$ pairs of overlapping buckets.
%Repeats the below?
%, bounding the
%difference between the true acceptance probability and the bucketed
%version.

\begin{proof}[Proof of Theorem\nobreakspace \ref {prop:bucketing_loss_bound}]
  Note that the bucketed payoff $\buck( % F(\x[1]), \dotsc, F(\x[t]), G(\y[t]), \dotsc,
  % G(\y[m])
  (F,G) \circ (\X,\Y))$ is equal to the true payoff $\payoff(% f(\x[1]), \ldots, f(\x[t]),%
  % g(\y[t+1]), \ldots, g(\y[m])
  (f,g) \circ (\X,\Y))$ except possibly when at least two elements in $(F,G) \circ (\X,\Y)$
  fall in an overlapping pair of buckets. % We proceed to bound this
  %probability.% of this happening by $m^2{\buckParameter}^{-\delta}/2$.
  Fix a pair of inputs, say $\super{\vx}{i}$ and $\super{\vy}{j}$; the proof is
  identical were we to choose two inputs from $\X$ or two from $\Y$.
  Let $a = F(\super{x}{i})$ and $b = G(\super{y}{j})$.  By Lemma\nobreakspace \ref {prop:bucket_overlap_bound}
  there are at most $2{\buckParameter}$ possible values $(a,b)$ such that the buckets
  indexed by $a$ and $b$ are overlapping. From Lemma\nobreakspace \ref {prop:hyperContractiveBound},
  the probability that $F(\x[i]) = a$ and $G(\y[j]) = b$ is at most
  ${\buckParameter}^{-1-\delta}$.  By a union bound, the two outputs $F(\x[i])$,
  $G(\y[j])$ consequently fall in overlapping buckets with probability at most
  $2{\buckParameter}^{-\delta}$.  As there are at most ${m \choose 2} \le m^2/2$ pairs
  of outputs, the proof is complete.
\qedendproof


\subsection{Soundness of the Dictatorship Test}\label{sec:test_soundness}

We now reap the benefits of bucketing and prove the decoupling
property of the dictatorship test alluded to in
Section\nobreakspace \ref {sec:dictatorshipTest}.  

\full{
\begin{theorem}
  \label{lemma:test bucketed soundness}
  For every predicate $\payoff$ and distribution $\baseDist$
  satisfying the conditions of Theorem\nobreakspace \ref {theorem:reduction soundness},
  and any noise rate $\gamma > 0$, projection $\pi: \lcRightLabelSet \to \lcLeftLabelSet$, and
  bucketing parameter ${\buckParameter}$, the following holds.  For any functions $f:
  Q_1^\lcLeftLabelSet \to \Z$, $g: Q_2^\lcRightLabelSet \to \Z$ with bucketing functions $F:
  Q_1^\lcLeftLabelSet \to [{\buckParameter}]$, $G: Q_2^\lcRightLabelSet \to [{\buckParameter}]$,
 \begin{align*}
  \abs{ \bVal(\dicD) -  \bVal(\dicDP) } \le \gamma^{-1/2}
 m^{1/2} 4^m {\buckParameter}^m \sum_{a, b \in \irange{{\buckParameter}}} \Coinf{\super{F}{a},
   \super{G}{b}}^{1/2}.
 \end{align*}
\end{theorem}

Recall that the decoupled version $\decoup{\baseDist}$ of a base
distribution $\baseDist$ is obtained by combining two independent
samples of $\baseDist$: one for the first $t$ coordinates and one
for the remaining.  A similar claim, as above, for the true acceptance
probabilities of the dictatorship test is now a simple corollary of
the above \MakeLowercase Theorem and Theorem\nobreakspace \ref {prop:bucketing_loss_bound}.  This will be
used later in extending the decoupling property to our general
inapproximability reduction.
}

\begin{lemma}
  \label{lemma:test soundness}
  For every predicate $\payoff$ and distribution $\baseDist$
  satisfying the conditions of Theorem\nobreakspace \ref {theorem:reduction soundness},
  and any noise rate $\gamma > 0$, projection $\pi: \lcRightLabelSet \to \lcLeftLabelSet$, and
  bucketing parameter ${\buckParameter}$, the following holds.  For any functions $f:
  Q_1^\lcLeftLabelSet \to \Z$, $g: Q_2^\lcRightLabelSet \to \Z$ with bucketing functions $F:
  Q_1^\lcLeftLabelSet \to [{\buckParameter}]$, $G: Q_2^\lcRightLabelSet \to [{\buckParameter}]$,
  \begin{equation}\begin{split} \Big\lvert \accP(&\dicD) -  \accP(\dicDP) \Big\rvert\\%
      &\le \gamma^{-1/2} m^{1/2} 4^m {\buckParameter}^m%
      \sum_{a, b \in \irange{{\buckParameter}}} \Coinf{\super{F}{a},
        \super{G}{b}}^{1/2} + 2 {\buckParameter}^{-\delta} m^2.
\end{split}
\end{equation}
\end{lemma}

% In particular, we can now make precise the notion of sharing
% influential variables: we say that $f$ and $g$ share an influential
% coordinate if $\sum_{a, b \in [{\buckParameter}]} \Coinf{\super{F}{a},
%   \super{G}{b}}^{1/2}$ is non-negligible (see
% Section~\ref{sec:realAnalysis} for the definition of $\Coinf{\cdot,
%   \cdot}$.  Using a variant of the invariance principle, we can then
% prove the following.
%
% Rajsekar: We do not actually say 'f and g share an influential coordinate in any theorem statement'

% For space reasons, the proof is deferred to \cref{sec:ocspInvariance}.
% Combining \cref{prop:bucketing_loss_bound,lemma:test bucketed
%   soundness} immediately gives the following, which characterizes the
% soundness properties of the dictatorship test.
% Rajsekar: moved to this section itself.

\def\finalDecouplingBound{ \gamma^{-1.5} m^{1/2} 4^m {\buckParameter}^{m+1} \val(\lc)^{1/2} + 2 {\buckParameter}^{-\delta} m^2 }

%\section{Decoupling Tables via Invariance}\
\label{sec:ocspInvariance}

% In this section we prove Theorem\nobreakspace \ref {lemma:test bucketed soundness},
% restated here for convenience.

% % This is incredibly ugly. /c
% \ifdefined\lncsVersion
% \spnewtheorem*{bucket-soundness-lemma}{Lemma}{\bf}{\it}
% \begin{bucket-soundness-lemma}[Theorem\nobreakspace \ref {lemma:test bucketed soundness} restated]
%   \else
%   \newtheorem*{bucket-soundness-lemma}{Theorem\nobreakspace \ref {lemma:test bucketed soundness} restated}
%   \begin{bucket-soundness-lemma}
% \fi %
% %
% For every predicate $\payoff$ and distribution $\baseDist$ satisfying
% the conditions of Lemma~\ref{theorem:reduction soundness}, and any noise
% rate $\gamma > 0$, projection $\pi: \lcRightLabelSet \to \lcLeftLabelSet$, and bucketing parameter
% ${\buckParameter}$, the following holds.  For any functions $f: Q_1^\lcLeftLabelSet \to \Z$, $g:
% Q_2^\lcRightLabelSet \to \Z$ with bucketing functions $F: Q_1^\lcLeftLabelSet \to [{\buckParameter}]$, $G: Q_2^\lcRightLabelSet
% \to [{\buckParameter}]$ it holds that
%     \[ \abs{ \bVal(\dicD) -  \bVal(\dicDP) } \le \gamma^{-1/2}
%     m^{1/2} 4^m {\buckParameter}^m \sum_{a, b \in \irange{{\buckParameter}}} \Coinf{\super{F}{a},
%       \super{G}{b}}^{1/2}
%     \]
%   \end{bucket-soundness-lemma}
%   Rajsekar: moved the section into where the lemma was originally
%   stated.  Thus commented.


The proof of the theorem is via an invariance-style theorem and uses
techniques found in the works of
Mossel~\cite{Mossel10}, Samorodnitsky and
Trevisan~\cite{SamorodnitskyT09}, and Wenner~\cite{Wenner12}. 
The first lemma essentially says that if a product of functions is
influential, then at least one of the involved functions is
influential. The second lemma decouples from pairwise independence and
mostly involves introducing new notation where the notion of
\emph{lifted} functions is the most alien: a large-side table $g: \irange{q_2}^\lcRightLabelSet \rightarrow
\R$ may equivalently be seen as the function $\lifted[\pi]{g} :
\lifted[\lcLeftLabelSet]{\Omega} \rightarrow \R$ where
$\lifted{\Omega} = \irange{q_2}^d$ contains the values of all $d$
coordinates in $\lcRightLabelSet$ projecting to the same coordinate in
$\lcLeftLabelSet$. $\lifted[\pi]{g}$ is called the lifted analogue of
$g$ with respect to the projection $\pi$ and one of the below lemmas
essentially says that if the lifted analogue of $g$ is influential for
a coordinate $i \in \lcLeftLabelSet$, then $g$ is influential in a
coordinate projecting to $i$. The lemma will be used to -- after
massaging the expression -- decoupling the small-side table from the
lifted analogues of the large-side table as a function of their
\CoinfTerm{}.


% Note, we previously also cited
% Lemma 3, Samorodnitsky and Trevisan~\cite{SamorodnitskyT09}; 
% however, although the proof is the same, they only stated the lemma
% for domains {-1,1}. /c
\begin{lemma}[Lemma 6.5, Mossel~\cite{Mossel10}]
\label{lemma:influence_of_product_to_sum_of_influences}
Let $f_1, \dotsc, f_t: \Omega^n \rightarrow [0,1]$ be arbitrary functions.
Then for any $j \in \irange{n}$, $\Inf[j]{\prod_{r=1}^t f_r} \leq t \sum_{r=1}^t \Inf[j]{f_r}$.
\end{lemma}


% \begin{lemma}
% \label{lemma:dict_indicator_invariance}
% Let $\mD$ be a distribution on the product space $\overbrace{\irange{q_1} \times \dotsm \times \irange{q_1}}^{t} \times \overbrace{\irange{q_2} \times \dotsm \times \irange{q_2}}^{m-t}$ such that each outcome space has a uniform marginal and $\Omega_r$ is independent of $\Omega_1 \times \dotsm \times \Omega_t$ for each $t < r \leq m$. Define $\decoup{\mD}$ as the distribution where $\Omega_1 \times \dotsm \times \Omega_t$ is drawn independently of $\Omega_{t+1} \times \dotsm \times \Omega_m$ from $\mD$.
% Consider functions $\{\super{F}{r}: \irange{q_1}^\lcLeftLabelSet \rightarrow [0,1]\}_{r=1}^t$, $\{\super{G}{r}: \irange{q_2}^\lcRightLabelSet \rightarrow [0,1]\}_{r=t+1}^m$, and $\pi$ a projection from $\lcRightLabelSet$ to $\lcLeftLabelSet$. Then,
% \begin{gather*}
% \abs{
% \Ex[\ntestDist{\mD}]{ \prod_{r=1}^t \super{F}{r}(\super{\rvvx}{r}) \prod_{r=t+1}^m \super{G}{r}(\super{\rvvy}{r}) }
% - \Ex[\ntestDist{\decoup{\mD}}]{ \prod_{r=1}^t \super{F}{r}(\super{\rvvx}{r}) \prod_{r=t+1}^m \super{G}{r}(\super{\rvvy}{r}) }
% }
% \\\leq
% \gamma^{-1/2} m^{1/2} 4^m \sum_{r,r' \in [{\buckParameter}]} \Coinf{\super{F}{r}, \super{G}{r'}}^{1/2}.
% \end{gather*}
% \end{lemma}
% Rajsekar this has been moved into the proof of the main theorem.


%\begin{remark}[Page 41, Wenner~\cite{Wenner12}; paraphrased]
%We also note that if one defines a variant of a function where
%collections of coordinates are ``clumped together'' as single coordinates
%with product domains, then the influence of a new coordinate is no greater than the total
%influence of the involved clumped-together coordinates. 
\begin{lemma}%[Page 41, Wenner~\cite{Wenner12}; paraphrased]
\label{lemma:lifted_influences_to_projected}
%~ Let a function $g: \Omega^\lcRightLabelSet \rightarrow
%~ \R$ and a projection $\pi: \lcRightLabelSet \rightarrow
%~ L$ be given where $\lcRightLabelSet = \lcLeftLabelSet \times \irange{d}$ and
%~ $\lifted[\pi]{g}: (\Omega^d)^\lcLeftLabelSet \rightarrow \R$ is
%~ suitably defined.
%~ %Then the influence of a coordinate $i \in
%~ %\lcLeftLabelSet$ translates naturally to the sum of influences $j \in
%~ %\lcRightLabelSet$ projecting to $i$.
%~ %By the definition of influence, $\Inf_i(\bar{g}^\pi) = \expectation[ \bfx_{-i} ]{ \variance[ \bfx_i ]{\bar g^\pi(\bfx) } }$. Turning to the decomposition of $g$,
%~ %\[\expectation[ \bfx_{-i} ]{ \variance[ \bfx_i ]{\bar g^\pi(\bfx) } }\]
%~ %Namely, we have
%~ Then, $ \Inf[i]{\lifted[\pi]{g}} \leq \sum_{j \varsuchthat \pi(j) = i} \Inf[j]{g}.$ % = \Inf[\pi^{-1}(i)]{g}
%~ This follows from the expression of influences in decompositions of $g$ which equals
%~ $\sum_{T \varsuchthat i \in \pi(T)} \Ex{g_T^2}$ in the former %two cases
%~ case and $\sum_T \card{T \cap \pi^{-1}(i)} \Ex{g_T^2}$ in the latter.% third.

Consider a function $g : \Omega^R \rightarrow \R$, a map $\pi : R \rightarrow L$,
and an arbitrary bijection $\varsigma : R \leftrightarrow ((l) \times \irange{\card{\pi^{-1}(l)}} )_{l \in L}$
such that for all $(i,j)$, $\exists_t \varsigma(j) = (i,t)$ iff $\pi(j) = i$.
Introduce $\Omega'_l \defeq \Omega^{\card{\pi^{-1}(l)}}$ and define
$g' : \prod_L \Omega'_l \rightarrow \R$ as $g'(\vec{z}) = g(\vec{y})$ where
$y_{j} = z_{\varsigma(j)}$.
%~ Consider a function $g : \Omega^R \rightarrow \R$ and a bijection $\varsigma$ between $L \times \irange{d}$ and $R$
%~ where $\card{R} = d \card{L}$. Define $g' : {\Omega'}^L$ for $\Omega' = \Omega \times \irange{d}$
%~ as $g'(\vz) = g(\vy)$ where $y_l = z_{\varsigma^{-1}(l)}$.
Then for every $i \in L$,
\[
\Inf[i]{g'} \le \sum_{j \in \pi^{-1}(i)} \Inf[j]{g}.
\]
\end{lemma}
\begin{proof}
Using definitions and the law of total conditional variance,
\[
\Inf[i]{g'} = \Ex[\Omega'_{-i}]{ \var_{\Omega'_i} g' } = \Ex[\{\Omega_l\}_{l: \not\exists_j \varsigma(i,j) = l}]{ \var_{\{\Omega_l\}_{l: \exists_j \varsigma(i,j) = l} } g}
\le \sum_{l: \exists_j \varsigma(i,j) = l} \Ex[\Omega_{-l}]{\var_{\Omega_l} g} = \sum_{l: \exists_j \varsigma(i,j) = l} \Inf[l]{g}.
\]
\end{proof}


\begin{theorem}[Theorem 3.21, Wenner~\cite{Wenner12}]
\label{prop:yindepxinvariance}
\newcommand*{\stdprobspace}{\cP = (\prod_{i=1}^m \Omega_i, \rmP)}
Consider functions $\{f^{(r)} \in \rmL^\infty(\Omega_r^n)\}_{r \in \oneirange{m}}$
on a probability space $\stdprobspace^{\otimes n}$, a set $M \subsetneq \oneirange{m}$,
and a collection $\cC$ of minimal sets\footnote{Minimal sets of dependent outcome spaces
in the sense that if $C \in \cC$, then the outcomes spaces of every strict subset of $C$ are independent.} 
$C \subseteq \oneirange{m}, C \nsubseteq M$ such that the spaces $\{ \Omega_i \}_{i \in C}$ are dependent.
Then,
\begin{gather*}
  \abs{\Ex{\prod_{r=1}^m f^{(r)}} - \prod_{r \notin M} \Ex{f^{(r)}}
    \Ex{\prod_{r \in M} f^{(r)}}} \\\leq 4^m \max_{C \in \cC} \sqrt{
    \min_{r' \in C} \Totinf{f^{(r')}} \sum_l \prod_{r \in C \setminus
      \{r' \} } \Inf[l]{f^{(r)}} } \prod_{r \notin C}
  \norm[\infty]{f^{(r)}}.
\end{gather*}
\end{theorem}


% The decoupling corollary is used in our soundness analysis to argue
% that if the \labelcover instance does not have a good labeling, then
% querying the two tables independently does not significantly affect
% the value of the protocol. We begin by proving the corollary as it
% is a simple consequence of the preceding lemma.%
% Rajsekar: this motivation now appears elsewhere.
% \iffalse
\paragraph{Proof of Theorem\nobreakspace \ref {lemma:test bucketed soundness}}
\begin{proof}
  We massage the expression $\bVal(\dicD)$ to a form suitable for applying
  Theorem\nobreakspace \ref {prop:yindepxinvariance}.  Recall that $\bucketInd[a]$ denotes
  the indicator of ``$F(\rvx) = a$'' and similarly $\bucketIndR[a]$ of
  ``$G(\rvy) = a$''. In terms of these indicators, $\bVal(\dicD)$ equals
\[
\sum_{\vec{a} \in \irange{{\buckParameter}}^t, \vec{b} \in \irange{{\buckParameter}}^{m-t}}
\wp (\vec{a}, \vec{b})
\Ex[ \dicD ]{ \prod_{r=1}^t \super{F}{a_r}(\super{\rvvx}{r}) \prod_{r=t+1}^m \super{G}{b_{r-t}}(\super{\rvvy}{r}) }.
\]
 Consequently, $\abs{ \bVal(\mD) -
  \bVal(\decoup{\mD}) }$ may be bounded from above by
\begin{gather}
\label{eq:erite8ygioj}
\sum_{\vec{a}, \vec{b}}
\wp (\vec{a}, \vec{b})
\abs{
\Ex[ \ntestDist{\mD} ]{ \prod_{r=1}^t \super{F}{a_r}(\super{\rvvx}{r}) \prod_{r=t+1}^m \super{G}{b_{r-t}}(\super{\rvvy}{r}) }
-
\Ex[ \ntestDist{\decoup\mD} ]{ \prod_{r=1}^t \super{F}{a_r}(\super{\rvvx}{r}) \prod_{r=t+1}^m \super{G}{b_{r-t}}(\super{\rvvy}{r}) }
}.
\end{gather}%

We note that $0 \le \wp \le 1$ and proceed to bound
 the difference in the summation for fixed arbitrary $(a,b)$.
To this end, we must make a slight change of notation as discussed previously.
The new notation may seem cumbersome; the high-level picture is that we group the first
set of functions into a single function, receiving a single argument over
a larger domain, and redefine the latter functions to take
arguments indexed by $\lcLeftLabelSet$ instead of $\lcRightLabelSet$.
Also recall that we intend to reduce from \labelcover{} instances
with projection degrees $d$ for some $d$.

Define $m' = m - t + 1$, $\Omega_1 = \irange{q_1}^t$, $\Omega_2 =
\dotsc = \Omega_{m'} = \irange{q_2}^d$. Let $\varsigma$ be a bijection $\lcLeftLabelSet \times
\irange{d} \leftrightarrow \lcRightLabelSet$ such that
$\pi(\varsigma(i, j')) = i$; \ie, for each $i \in L$, we group together the $d$ coordinates in $R$
mapping to $i$. Introduce the distribution
$\Omega_1^\lcLeftLabelSet \times \dotsm \times \Omega_{m'}^\lcLeftLabelSet
\ni (\rvv{w}, \super{\rvvz}{2}, \dotsc, \super{\rvvz}{m'}) \sim
\cR(\mu)$ which samples $(\super{\rvvx}{1}, \dotsc, \super{\rvvx}{t},
\super{\rvvy}{t+1}, \dotsc, \super{\rvvy}{m})$ from $\dicD$, setting
$\rv{w}_{i,r} = \super{\rvx}{r}_i$ and $\super{\rvz}{r}_i = (
\super{\rvy}{r}_{\varsigma^{-1}(i,j')} )_{j'=0}^{d-1}$. Let $W(\vec{w})
\defeq \prod_{r=1}^t
(\noiseop_{\pcomp{\gamma}}\super{F}{a_r})(\super{\vx}{r})$ where
$\super{x}{r}_i = w_{i,r}$ and similarly, for $2 \leq r \leq m'$, call
the lifted function $\super{H}{r}: \Omega_r^L \rightarrow \R$
defined as $\super{H}{r}(\vz) =
(\noiseop_{\pcomp{\gamma}}\super{G}{b_{r-1}})(\vy)$ where
$y_{\varsigma(i,j')} = z_{i,j'}$. With this new notation, the
difference within the summation in (\ref{eq:erite8ygioj}) is
\begin{gather}
\label{eq:dfsjkddopu8390}
\abs{
\Ex[\cR(\mD)]{ W(\rvv{w}) \prod_{r=2}^{m'} \super{H}{r}(\super{\rvvz}{r}) }
- \Ex[\cR(\decoup{\mD})]{ W(\rvv{w}) \prod_{r=2}^{m'} \super{H}{r}(\super{\rvvz}{r}) }
}.
\end{gather}

We note that $\cR$ is a product distribution
$\cR = \mu^{\otimes \lcLeftLabelSet}$ for some $\mu$ and for any
$2 \leq r \leq m'$, $\Omega_r$ is independent of $\Omega_1$ due to
the premise that $\cD_r$ independent of $\cD_{\le t}$.
Choosing $M = \{2, \dotsc, m'\}$, minimal indices $\cC$ of dependent sets in
$\mu$ not contained in $M$ contains 1 and at least two elements from
$M$, \ie, $C \in \cC$ implies $1,e,e' \in C$ for some
$e \neq e' \in \{2,\dotsc,m'\}$.

Applying Theorem\nobreakspace \ref {prop:yindepxinvariance} and choosing $r' \neq 1$ bounds the difference (\ref{eq:dfsjkddopu8390}) by
\begin{gather}
\label{eq:i35r90weyfiosjdfiop}
4^m \sqrt{ \max_{C \in \cC} \min_{1 \neq e \in C} \Totinf(\super{H}{e}) \sum_i \Inf[i]{W} \prod_{t \in C \setminus \{1, e \} } \Inf[i]{\super{H}{t}} }
\prod_{r \notin C} \norm[\infty]{\super{H}{r}}.
\end{gather}

As we assumed the codomain of the studied functions $\{\super{G}{r}\}_r$ to be $[0,1]$ the same holds for $\{\super{H}{r}\}_r$ and consequently the influences and infinity norms in (\ref{eq:i35r90weyfiosjdfiop}) are upper-bounded by one on account of Lemma\nobreakspace \ref {lemma:influence_variance_bound}, yielding at most
\begin{gather}
\label{eq:34950wtuefsdo}
(\ref{eq:i35r90weyfiosjdfiop}) \leq 4^m \left( \max_{e \neq 1} \Totinf (\super{H}{e}) \cdot \max_{e \neq 1} \sum_i \Inf[i]{W} \Inf[i]{\super{H}{e}} \right)^{1/2}.
\end{gather}

We recall that $W = \prod_{r=1}^t \noiseop_{\pcomp{\gamma}} \super{F}{a_r}$ and hence by Lemma\nobreakspace \ref {lemma:influence_of_product_to_sum_of_influences},
\[
\Inf[i]{W} \leq t \sum_{r=1}^t \Inf[i]{\noiseop_{\pcomp{\gamma}} \super{F}{a_r}} = t \sum \Inf[i][\pcomp{\gamma}]{\super{F}{a_r}}.
\]
Similarly, Lemma\nobreakspace \ref{lemma:lifted_influences_to_projected} implies that
\[
\Inf[i]{\super{H}{e}} \leq \sum_{j \in \pi^{-1}(i)} \Inf[j]{\noiseop_{\pcomp{\gamma}}\super{G}{b_{e-1}}} = \sum_{j \in \pi^{-1}(i)} \Inf[j][\pcomp{\gamma}]{ \super{G}{b_{e-1}}}.
\]
 Returning to \expref{Equation}{eq:34950wtuefsdo}, we have the bound
\begin{gather}\label{eq:49tyh0sfsifs}
(\ref{eq:34950wtuefsdo}) \leq 4^m \left( t \max_{e > t} \Totinf[\pcomp{\gamma}](\super{G}{b_e}) \cdot \max_{e > t} \sum_i \sum_{r=1}^t \Inf[i][\pcomp{\gamma}]{\super{F}{a_r}} \sum_{j \in \pi^{-1}(i)} \Inf[j][\pcomp{\gamma}]{\super{G}{b_e}} \right)^{1/2}.
\end{gather}
Using $t \le m$; $\max_e \dotsc \le \sum_e \dotsc$ for non-negative expressions; bounding the total noisy influence by $\gamma^{-1}$ from Lemma\nobreakspace \ref {lemma:total_noisy_influence_bound};
and identifying the inner sum as a \CoinfTerm, we establish the desired bound on the considered difference as
\begin{gather*}
(\ref{eq:dfsjkddopu8390}) \leq (\ref{eq:49tyh0sfsifs}) \leq 4^m \left( t \gamma^{-1} \sum_{r,e} \Coinf{\super{F}{a_r}, \super{G}{b_e}} \right)^{1/2}
\leq \gamma^{-1/2} m^{1/2} 4^m \sum_{r,r'} \Coinf{\super{F}{a_r}, \super{G}{b_{r'}}}^{1/2}.
\end{gather*}
Finally, as we noted before, since $0 \le \wp \le 1$,
and since there are at most $\Gamma^m$ terms in the summation,
(\ref{eq:erite8ygioj}) is at most
\begin{align*}
% \sum_{\vec{a}, \vec{b}}
% \wp (\vec{a}, \vec{b})
% \abs{
% \gamma^{-1/2} m^{1/2} 4^m \sum_{r, r' \in \irange{{\buckParameter}}} \Coinf{\super{F}{a_r}, \super{G}{a_r'}}^{1/2}
% }
% \\\leq 
\gamma^{-1/2} m^{1/2} 4^m {\buckParameter}^m \sum_{a, b \in \irange{{\buckParameter}}} \Coinf{\super{F}{a}, \super{G}{b}}^{1/2}.
\end{align*}
\qedendproof




% We wish to apply Theorem\nobreakspace \ref {prop:yindepxinvariance} to establish
% \cref{lemma:dict_indicator_invariance}. 

% \begin{proof}[Proof of \cref{lemma:dict_indicator_invariance}]


% \qedendproof


\subsection{Soundness of the Reduction}
\label{sec:reduction soundness}

With the soundness for the dictatorship test in place, proving the
soundness of the reduction (Theorem\nobreakspace \ref {theorem:reduction soundness}) is a
relatively standard task of % , and this last component is deferred to
% Appendix~\ref{sec:reduction soundness} for space reasons.
%
%
% To prove the soundness of the reduction, we proceed by
constructing noisy-influence decoding
strategies. % in a rather standard
% way.  
% We restate this theorem for the readers convenience.

% \ifdefined\lncsVersion
% 	\spnewtheorem*{reduction-soundness-theorem}{Theorem}{\bf}{\it}
% 	\begin{reduction-soundness-theorem}[Theorem\nobreakspace \ref {theorem:reduction soundness} restated]
% \else
% 	\newtheorem*{reduction-soundness-theorem}{Theorem~\ref{theorem:reduction soundness} restated}
% 	\begin{reduction-soundness-theorem}
% \fi
%   Suppose that $\baseDist$ over $Q_1^t \times Q_2^{m-t}$ satisfies the following properties.
%   \begin{itemize}
%   \item $\baseDist$ has uniform marginals.
%   \item For every $i > t$, $\baseDist_i$ is independent of $\baseDist_{\le t}$.
%   \item $\baseDist_{\le t}$ and $\baseDist_{> t}$ are invariant under
%     permutation of coordinates (in other words, for every $x \in
%     Q_1^t$ and permutation $\pi$ on $\{1, \ldots, t\}$,
%     $\baseDist_{\le t}(x) = \baseDist_{\le t}(x \circ \pi)$ and
%     similarly for $\baseDist_{> t}$).
%   \end{itemize}
%   For every $\epsilon > 0$ and every $\gamma > 0$ there exists $\epsilon_{LC} > 0$ such that if $\val(\mathcal{L}) \le \epsilon_{LC}$
%   then for every assignment $\{f_u\}_{u \in U}$, $\{g_v\}_{v \in V}$ to $\instance$, it holds that
%   \[
%   \E_{\rv{e} = (u,v) \in E} [ \accP(\dictDist[\gamma]_{\pi_e}(\baseDist)) ] \le \E_{\rv{e} = (u,v) \in E} [ \accP(\dictDist[\gamma]_{\pi_e}(\decoup{\baseDist})) ] + \epsilon
%   \]
% \end{reduction-soundness-theorem}

The proof follows immediately from the more general estimate given
in the following lemma by taking ${\buckParameter} = \ceil{(4m^2/\epsilon)^{1/\delta}}$ and
then $\epsilon_{LC} = \left(\frac{\epsilon \gamma^{3/2}}{m^{1/2} 4^m
    {\buckParameter}^{m+1}}\right)^2$.
\newcommand{\strat}[1][{}]{\Psi_{#1}}%
\begin{lemma}\label{prop:expBound} Given an \labelcover instance $\lcDesc$
  and a collection of functions, $f_u: Q_1^L \to \Z$ for $u \in
  \lcLeft$; $g_v: Q_2^R \to \Z$ for $v \in \lcRight$, and ${\buckParameter}$,
  $\gamma$, $\delta$ as in this section,
  %  \[ \val(\lc) \ge \left[ \frac{\Ex[e \in \lcEdges]{\left|%
  %        \accP(\dictDist[\gamma]_{\pi_e}(\baseDist)) -%
  %        \accP(\dictDist[\gamma]_{\pi_e}(\decoup{\baseDist}))\right|}%
  %    - 4{\buckParameter}^{-\delta}m^2} {\gamma^{-1/2} m^{1/2} 4^m {\buckParameter}^m} \right]^2 \]
    \[
    \Ex[u,v \sim \lcEdges]{ \left| \accP[f_u, g_v](\dicD) - \accP[f_u, g_v](\dicDP) \right|} \le
    \finalDecouplingBound.
    \]
\end{lemma}

\begin{proof}
  For a function $f: Q_1^L \to \Z$  define
  a distribution $\strat(f)$ over $L$ as follows.  First pick $a \sim \irange{{\buckParameter}}$ uniformly, then pick $l \in L$ with probability $\gamma \cdot \infl(\bucketInd[a]_v)$ and otherwise an arbitrary label.  
  Note that by Lemma\nobreakspace \ref {prop:totInfBound}, $\sum_{l \in L} \infl(\bucketInd[a]_u) \le 1/\gamma$ and so picking $l \in L$ with the given probabilities is possible.  Define $\strat(g)$ over
  $R$ for $g: Q_2^R \to \Z$ similarly.  
  Now define a labeling of $\mathcal{L}$ by, for each $u \in U$
  (resp.~$v \in V$), sampling a label from $\strat(f_u)$
  (resp.~$\strat(g_v)$) independently.
  
  For an edge $e = (u,v) \in \lcEdges$, the probability that $e$ is satisfied by the labeling equals $\prob{ \pi_e(\strat(f_u)) = \strat(g_v)}$, which can be lower-bounded by
  \begin{gather*}
    \sum_{i,j \varsuchthat \pi_e(j) = i} \gamma^2
    \Ex[a,b \in \irange{{\buckParameter}}]{ \Inf[i][1-\gamma]{\bucketInd[a]_u} \Inf[j][1-\gamma]{\bucketIndR[b]_v} }
    = (\gamma/{\buckParameter})^2
    \sum_{a,b} \Coinf[\pi_e]{\bucketInd[a]_u, \bucketIndR[b]_v }.
  \end{gather*}
  Taking the expectation over all edges of $\mathcal{L}$, we get that the fraction of satisfied constraints is
  \[
  (\gamma/{\buckParameter})^2 \E_{e = (u,v)}\left[\sum_{a,b} \Coinf[\pi_e]{\bucketInd[a]_u,
    \bucketIndR[b]_v }\right] \le \val(\mathcal{L}),
\] and by concavity of the $\sqrt{\cdot}$ function, this implies that
\[
\E_{e = (u,v)}\left[\sum_{a,b}
  \Coinf[\pi_e]{\bucketInd[a]_u,\bucketIndR[b]_v }^{1/2}\right] \le
{\buckParameter}\gamma^{-1} \val(\mathcal{L})^{1/2}.
\]
  Plugging this bound on the total cross influence into the soundness
  for the test, Lemma\nobreakspace \ref {lemma:test soundness}, we obtain
  \[
  \Ex[e = (u,v)]{ \left| \accP[f_u, g_v](\dicDpie) - \accP[f_u, g_v](\dicDPpie) \right|} \le
  \finalDecouplingBound,
  \]
  as desired.
\qedendproof





%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "orderingNPHardness"
%%% End: 

